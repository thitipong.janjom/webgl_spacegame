using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorController : MonoBehaviour
{
    public GameObject[] meteorPrefab;

    Queue<GameObject> availbleMeteors = new Queue<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i < meteorPrefab.Length; i++)
        {
            availbleMeteors.Enqueue(meteorPrefab[i]);
        }
        //availbleMeteors.Enqueue(meteorPrefab[0]);
        //availbleMeteors.Enqueue(meteorPrefab[1]);
        //availbleMeteors.Enqueue(meteorPrefab[2]);
        //availbleMeteors.Enqueue(meteorPrefab[3]);
        //availbleMeteors.Enqueue(meteorPrefab[4]);

        InvokeRepeating("MoveMeteorDown", 0, 20f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void MoveMeteorDown()
    {
        EnqueueMeteors();
        if (availbleMeteors.Count == 0)
            return;
        GameObject aMeteor = availbleMeteors.Dequeue();

        aMeteor.GetComponent<Meteors>().isMoving = true;

    }

    public void EnqueueMeteors()
    {
        foreach(GameObject aMeteor in meteorPrefab)
        {
            if ((aMeteor.transform.position.y < 0) && (!aMeteor.GetComponent<Meteors>().isMoving))
            {
                aMeteor.GetComponent<Meteors>().ResetPosition();

                availbleMeteors.Enqueue(aMeteor);
            }
        }
    }
}
