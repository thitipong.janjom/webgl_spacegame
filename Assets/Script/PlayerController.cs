using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    [Header("Reference")]
    public GameObject bulletPrefab;
    public GameObject[] bulletPosition;
    public GameObject GameManager;
    public Text lifeText;
    public AudioSource audioPlayer;
    public GameObject shieldPaticle;

    [Header("Player Attribute")]
    public int life;
    const int maxLife = 3;
    public int shield;
    const int maxShield = 1;
    public float moveSpeed;
    public bool tripleBulletMode;
    public float durationTripleBullet;
    const float maxTripleBulletMode = 10;

    [Header("Animation")]
    public GameObject explosionAnim;

    // Start is called before the first frame update
    void Start()
    { 
        GameManager = GameObject.Find("GameManager");
        audioPlayer = GameObject.Find("Player_Ship").GetComponent<AudioSource>();
        lifeText = GameObject.Find("Life_text").GetComponent<Text>();
        shieldPaticle = GameObject.Find("ShildParticle");
        for (int i = 0; i < 3; i++)
        {
            bulletPosition[i] = GameObject.Find("BulletPos"+i.ToString());
        }
    }

    // Update is called once per frame
    void Update()
    {
        float moveX = Input.GetAxisRaw("Horizontal");
        float moveY = Input.GetAxisRaw("Vertical");

        Vector2 moveDirection = new Vector2(moveX, moveY).normalized;

        Move(moveDirection);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Fire();
        }

        if (tripleBulletMode)
        {
            durationTripleBullet -= Time.deltaTime;
            if (durationTripleBullet <= 0)
            {
                tripleBulletMode = false;
            }
        }
    }

    private void Move(Vector2 direction)
    {
        Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
        Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));

        max.x = max.x - 0.5f;
        min.x = min.x + 0.5f;

        max.y = max.y - 0.75f;
        min.y = min.y + 0.75f;

        Vector2 pos = transform.position;

        pos += direction * moveSpeed * Time.deltaTime;

        pos.x = Mathf.Clamp(pos.x, min.x, max.x);
        pos.y = Mathf.Clamp(pos.y, min.y, max.y);

        transform.position = pos;
    }

    private void Fire()
    {
        audioPlayer.Play();
        GameObject bullet = Instantiate(bulletPrefab);
        bullet.transform.position = bulletPosition[0].transform.position;

        if (tripleBulletMode)
        {
            GameObject bullet1 = Instantiate(bulletPrefab, bulletPosition[1].transform.position, Quaternion.identity);
            //bullet1.transform.position = bulletPosition[1].transform.position;
            //bullet1.transform.rotation = bulletPosition[1].transform.rotation;
            GameObject bullet2 = Instantiate(bulletPrefab, bulletPosition[2].transform.position, Quaternion.identity);
            //bullet2.transform.position = bulletPosition[2].transform.position;
            //bullet2.transform.rotation = bulletPosition[2].transform.rotation;
        }
        
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if((collision.tag == "EnemyBullet") || (collision.tag == "EnemyShip"))
        {
            if (shield == 1)
            {
                shield--;
                GameManager.GetComponent<GameManagers>().shieldUI.SetActive(false);
                shieldPaticle.SetActive(false);
                PlayExplosionAnim();
            }
            else
            {
                PlayExplosionAnim();
                life--;
                lifeText.text = life.ToString();
            }

            if(life == 0)
            {
                GameManager.GetComponent<GameManagers>().SetGameManagerState(GameManagers.GameManagerState.GameOver);
                gameObject.SetActive(false);
            }  
        }

        if(collision.tag == "Shield")
        {
            if (shield == 0)
            {
                shield++;
                GameManager.GetComponent<GameManagers>().shieldUI.SetActive(true);
                shieldPaticle.SetActive(true);
            }
        }

        if (collision.tag == "Heal")
        {
            if (life < 3)
            {
                life++;
                lifeText.text = life.ToString();
            }
        }

        if (collision.tag == "TripleBullet")
        {
            tripleBulletMode = true;
            durationTripleBullet = maxTripleBulletMode;
        }
    }

    void PlayExplosionAnim()
    {
        GameObject anim = Instantiate(explosionAnim);

        anim.transform.position = transform.position;
    }

    public void Init()
    {
        life = maxLife;
        shield = maxShield;
        tripleBulletMode = false;
        lifeText.text = life.ToString();
        transform.position = new Vector2(0, 0);
        gameObject.SetActive(true);
        shieldPaticle.SetActive(true);
    }

}
