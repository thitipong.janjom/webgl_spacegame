using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UFO_Controller : MonoBehaviour
{
    public float speed;
    public GameObject explosionAnim;
    public int plusScore;
    public int life = 3;
    GameObject scoreText;

    // Start is called before the first frame update
    void Start()
    {
        scoreText = GameObject.FindGameObjectWithTag("ScoreText");
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 pos = transform.position;

        pos = new Vector2(pos.x, pos.y - speed * Time.deltaTime);

        transform.position = pos;

        Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));

        if (transform.position.y < min.y)
        {
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if ((collision.tag == "PlayerBullet") || (collision.tag == "PlayerShip"))
        {
            if (life > 1)
                life--;
            else
            {
                PlayExplosionAnim();
                scoreText.GetComponent<GameScore>().Score += plusScore;
                Destroy(gameObject);
            }
        }
    }

    void PlayExplosionAnim()
    {
        GameObject anim = Instantiate(explosionAnim);

        anim.transform.position = transform.position;
    }
}
