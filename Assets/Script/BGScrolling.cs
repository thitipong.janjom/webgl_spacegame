using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BGScrolling : MonoBehaviour
{
    public Renderer bg;
    public float bgSpeed;

    // Start is called before the first frame update
    void Start()
    {
        bg = GameObject.Find("BG_Scrolling").GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        bg.material.mainTextureOffset += new Vector2(0, bgSpeed * Time.deltaTime);
    }
}
