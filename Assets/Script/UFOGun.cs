using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UFOGun : MonoBehaviour
{
    UFOGun[] ufogun;
    public UFOBullet bullet;
    Vector2 direction;

    // Start is called before the first frame update
    void Start()
    {
        ufogun = transform.GetComponentsInChildren<UFOGun>();
        InvokeRepeating("UFOShoot", 0, 1.5f);
    }

    // Update is called once per frame
    void Update()
    {
        direction = (transform.localRotation * Vector2.down).normalized;
    }

    public void UFOShoot()
    {
        foreach(UFOGun gun in ufogun)
        {
            gun.FireEnemyBullet();
        }
    }

    public void FireEnemyBullet()
    {
        GameObject go = Instantiate(bullet.gameObject, transform.position, Quaternion.identity);
        UFOBullet goBullet = go.GetComponent<UFOBullet>();
        goBullet.direction = direction;
    }
}
