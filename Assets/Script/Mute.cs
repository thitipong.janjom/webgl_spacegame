using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mute : MonoBehaviour
{
    public Sprite[] image;
    public AudioSource bgSound;
    public bool IsMute;
    // Start is called before the first frame update
    void Start()
    {
        bgSound = GameObject.Find("BGMusic").GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void muteUnmute()
    {
        if (!IsMute)
        {
            bgSound.mute = true;
            gameObject.GetComponent<Image>().sprite = image[1];
            IsMute = true;
        }
        else
        {
            bgSound.mute = false;
            gameObject.GetComponent<Image>().sprite = image[0];
            IsMute = false;
        }
    }
}
