using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGun : MonoBehaviour
{
    public GameObject EnemyBullet;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("FireEnemyBullet", 0, 1.5f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void FireEnemyBullet()
    {
        GameObject playerShip = GameObject.Find("Player_Ship");

        if (playerShip != null)
        {
            GameObject bullet = Instantiate(EnemyBullet);

            bullet.transform.position = transform.position;

            Vector2 direction = playerShip.transform.position - bullet.transform.position;

            bullet.GetComponent<EnemyBullet>().SetDirection(direction);
        }
    }
}
