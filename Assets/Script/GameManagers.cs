using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagers : MonoBehaviour
{
    public GameObject playBT;
    public GameObject playerShip;
    public GameObject enemySpawner;
    public GameObject itemsSpawner;
    public GameObject gameOverPanel;
    public GameObject scoreText;
    public GameObject shieldUI;
    public GameObject logoUI;
    public GameObject muteButton;

    public enum GameManagerState
    {
        Opening,
        Gameplay,
        GameOver
    }

    public GameManagerState GMstate;

    // Start is called before the first frame update
    void Start()
    {
        //playerShip = GameObject.Find("Player_Ship");
        enemySpawner = GameObject.Find("EnemySpawn");
        itemsSpawner = GameObject.Find("ItemsSpawner");
        scoreText = GameObject.Find("Score_text");
        shieldUI = GameObject.Find("Shield_UI");
        logoUI = GameObject.Find("LogoUI");
        muteButton = GameObject.Find("Mute");
        GMstate = GameManagerState.Opening;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void UpdateGameState()
    {
        switch (GMstate)
        {
            case GameManagerState.Opening:
                gameOverPanel.SetActive(false);
                playBT.SetActive(true);
                shieldUI.SetActive(false);
                logoUI.SetActive(true);
                muteButton.SetActive(true);
                break;

            case GameManagerState.Gameplay:
                scoreText.GetComponent<GameScore>().Score = 0;
                playBT.SetActive(false);
                shieldUI.SetActive(true);
                logoUI.SetActive(false);
                muteButton.SetActive(false);
                playerShip.GetComponent<PlayerController>().Init();
                enemySpawner.GetComponent<EnemySpawn>().StartSpawn();
                itemsSpawner.GetComponent<ItemSpawner>().StartSpawn();
                break;

            case GameManagerState.GameOver:
                enemySpawner.GetComponent<EnemySpawn>().StopSpawn();
                itemsSpawner.GetComponent<ItemSpawner>().StopSpawn();
                gameOverPanel.SetActive(true);
                Invoke("ChangeToOpeningState", 8f);
                break;
        }
    }

    public void SetGameManagerState(GameManagerState state)
    {
        GMstate = state;
        UpdateGameState();
    }

    public void StartGamePlay()
    {
        GMstate = GameManagerState.Gameplay;
        UpdateGameState();
    }

    public void ChangeToOpeningState()
    {
        SetGameManagerState(GameManagerState.Opening);
    }
}
