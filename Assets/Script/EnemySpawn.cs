using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour
{
    public GameObject[] enemyPrefab;
    public float maxSpawnRate;
    public float resetMaxSpawnRate;
    public float spawnEverySecond;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SpawnEnemy()
    {
        Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));

        Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));
        int enemyNumber = Random.Range(0, 2);
        Debug.Log("Enemy No. " + enemyNumber);
        GameObject Enemy = Instantiate(enemyPrefab[enemyNumber]);
        Enemy.transform.position = new Vector2(Random.Range(min.x, max.x), max.y);

        NextSpawn();
    }

    public void NextSpawn()
    {
        float spawnInSeconds;

        if (maxSpawnRate > 1f)
        {
            spawnInSeconds = Random.Range(1f, maxSpawnRate);
        }
        else
            spawnInSeconds = 1f;
        Invoke("SpawnEnemy", spawnInSeconds);
    }

    public void IncreaseSpawnRate()
    {
        if (maxSpawnRate > 1f)
        {
            maxSpawnRate--;
        }
        if(maxSpawnRate == 1f)
        {
            CancelInvoke("IncreaseSpawnRate");
        }
    }

    public void StartSpawn()
    {
        maxSpawnRate = resetMaxSpawnRate;
        Invoke("SpawnEnemy", maxSpawnRate);
        InvokeRepeating("IncreaseSpawnRate", 0f, spawnEverySecond);
    }

    public void StopSpawn()
    {
        CancelInvoke("SpawnEnemy");
        CancelInvoke("IncreaseSpawnRate");
    }
}
