using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawner : MonoBehaviour
{
    public GameObject[] itemsPrefab;
    public float maxSpawnRate;
    public float resetMaxSpawnRate;
    public int randomItems;
    public int maxRange;
    public float maxSpawnNextRound;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SpawnItems()
    {
        Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));

        Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));

        GameObject items = Instantiate(itemsPrefab[randomItems]);
        items.transform.position = new Vector2(Random.Range(min.x, max.x), max.y);

        Debug.Log("Spawn Item");

        NextSpawn();
    }

    public void NextSpawn()
    {
        float spawnInSeconds;
        spawnInSeconds = Random.Range(1f, maxSpawnNextRound);
        Debug.Log(spawnInSeconds);
        randomItems = Random.Range(0, maxRange);
        Debug.Log(randomItems);
        Invoke("SpawnItems", spawnInSeconds);
    }

    public void IncreaseSpawnRate()
    {
        if (maxSpawnRate > 1f)
        {
            maxSpawnRate--;
        }
        if (maxSpawnRate == 1f)
        {
            CancelInvoke("IncreaseSpawnRate");
        }
    }

    public void StartSpawn()
    {
        maxSpawnRate = resetMaxSpawnRate;
        Invoke("SpawnItems", maxSpawnRate);
        InvokeRepeating("IncreaseSpawnRate", 0f, 30f);
    }

    public void StopSpawn()
    {
        CancelInvoke("SpawnItems");
        CancelInvoke("IncreaseSpawnRate");
    }
}
